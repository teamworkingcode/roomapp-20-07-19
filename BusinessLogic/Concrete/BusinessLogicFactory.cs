﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelFiles.Abstract;
namespace BusinessLogic
{
    public class BusinessLogicFactory : Creator
    {
        public BusinessLogicFactory() : base()
        {

        }
        public override T CreateModel<T>()
        {
            return (T)Activator.CreateInstance(typeof(T));
        }
    }
}
