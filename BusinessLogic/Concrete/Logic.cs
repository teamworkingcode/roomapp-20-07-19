﻿using ModelFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using ModelFiles.Enums;
using ModelFiles.Interfaces;
using ModelFiles.Concrete;

namespace BusinessLogic
{
    public class Logic : IBusinessLogic
    {
        private BusinessLogicFactory _creator;
        public Logic(BusinessLogicFactory Creator)
        {
            _creator = Creator;
        }

        private T BuildModel<T>() where T : new()
        {
            return _creator.CreateModel<T>();
        }

        public IDimensions ReturnModel(Operation operation)
        {
            switch(operation)
            {
                case Operation.Area:
                    return BuildModel<Floor>();
                case Operation.RoomVolume:
                    return BuildModel<RoomVolume>();
            }
            return null;
        }



    }
}
