﻿using ModelFiles.Enums;
using ModelFiles.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public interface IBusinessLogic
    {
        IDimensions ReturnModel(Operation operation);
    }
}
