﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModelFiles;
using BusinessLogic;
using FloorApp.Models;
using ModelFiles.Interfaces;

namespace FloorApp.Controllers
{
    public class HomeController : Controller
    {
        private IBusinessLogic _logic;

        public HomeController(IBusinessLogic Logic)
        {
            _logic = Logic;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ExistingUser(int UserID)
        {
            ViewBag.ExistingUser = true;
            
            return View("Index");
        }

        [HttpGet]
        public ActionResult GuestPage()
        {
            ViewBag.Greeting = "Hello " + Environment.MachineName;

            return PartialView("GuestPage", null);

        }

        //this action is called in order to instanciate the models for the editor templates views. if null loads the option that the user may select
        [HttpPost]
        public ActionResult OperationSwitch(int OpEnum)
        {
            if (OpEnum > 0)
            {
                IDimensions _res = _logic.ReturnModel((ModelFiles.Enums.Operation)OpEnum);
                return PartialView("OperationSwitch", _res);
            }
            return PartialView("GuestPage", null);
        }

        [HttpGet]
        public IActionResult FloorDimensions()
        {
            return PartialView();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
