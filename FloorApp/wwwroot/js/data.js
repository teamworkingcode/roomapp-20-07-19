﻿function projectData() {
    var entity;
    var makeCall = function (url, type, data, callback, callbackArgs) {
        var http = new XMLHttpRequest();
        var formData = buildForm(data);
        http.open(type, url, true);
        http.responseType = 'text';
        http.onreadystatechange = function (d) {
            if (d.target.status === 200 && d.target.response.length > 0) {
                entity = d.target.response;
                if (callback) {
                    callbackArgs.push(entity);
                    callback.apply(null, callbackArgs);
                }
            }
        };
        http.send(formData);
    };

    var buildForm = function (data) {
        var formData = new FormData();
        if (data !== null && data !== undefined) {
            for (var i = 0; i < data.length; i++) {
                var key = data[i].key;
                var value = data[i].value;
                formData.append(key, value);
            }
        }
        return formData;
    };

    return {
        makeCall: makeCall
    };
}