﻿function functions() {

    var promptConfirm = function (callback, callbackArgs, message) {
        var r = confirm(message);
        if (r === true) {
            if (callback !== null) {
                callback.apply(null, callbackArgs);
            }
        } else {
            alert('You have cancelled this process.');
        }
    };

    var replaceElement = function (parentSelector, exists, entity) {
        var parent = document.querySelector(parentSelector);
        var existing = document.querySelector(parentSelector + ' ' + exists);
        if (existing) {
            existing.innerHTML = entity.trim();
            return;
        } else {
            var section = document.createElement('section');
            section.innerHTML = entity.trim();
            var newEntity = section.firstChild;
            parent.appendChild(newEntity);
            return;
        }
    };

    var calculateArea = function (Width, Height) {

        if (Width <= 0 || Height <= 0) {
            return 'Out of range';
        }
        var res = Width * Height;
        return res;
    };

    var calculateVolume = function (Length, Width, Height) {
        if (Length <= 0 || Width <= 0 || Height <= 0) {
            return 'Out of Range';
        }
        var res = Width * Height * Length;

        return res;
    };
        
    var updateTable = function (tableName, Model) {
        var template = document.querySelector(tableName + ' ' + 'tbody tr');
        var clone = template.cloneNode(true);
        for (var i = 0; i < clone.children.length; i++) {
            for (var name in Model) {
                if (name === clone.children[i].getAttribute('name')) {                    
                    clone.children[i].innerText = Model[name];
                }
            }
        }
        document.querySelector(tableName + ' tbody').appendChild(clone);
    };

    return {
        promptConfirm: promptConfirm,
        replaceElement: replaceElement,
        calculateArea: calculateArea,
        calculateVolume: calculateVolume,
        updateTable: updateTable
    };
}