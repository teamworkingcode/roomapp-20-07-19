﻿var main = {};
main = (function (data, fun) {
    var clickEvents = function (event) {
        var target = event.target;
        var action = target.getAttribute('data-action');
        var enuma = 0;
        if (action !== null) {
            switch (action) {
                case 'test':
                    alert('you have clicked a button');
                    break;
                case 'guest-page':
                    enuma = target.getAttribute('data-value');
                    data.makeCall('/Home/OperationSwitch/', 'POST', [{ key: 'OpEnum', value: Number(enuma) }], fun.replaceElement, ['.content-section #workingArea', '#operationArea']);
                    break;
                case 'login':
                    fun.promptConfirm(null, null, 'This login page is pointless. Hire me and I will finish it. ;)');
                    break;
                case 'calculate-area':
                    var width = document.querySelector('#areaForm input[name="Width"]');
                    var height = document.querySelector('#areaForm input[name="Height"]');
                    var Floor = {
                        Width: Number(width.value),
                        Height: Number(height.value),
                        Value: fun.calculateArea(width.value, height.value)
                    };
                    fun.updateTable('.table', Floor);
                    break;
                case 'calculate-volume':
                    var vwidth = document.querySelector('#areaForm input[name="Width"]');
                    var vheight = document.querySelector('#areaForm input[name="Height"]');
                    var length = document.querySelector('#areaForm input[name="Length"]');

                    var Area = {
                        Width: Number(vwidth.value),
                        Height: Number(vheight.value),
                        Length: Number(length.value),
                        Value: fun.calculateVolume(length.value, vwidth.value, vheight.value)
                    };
                    fun.updateTable('.table', Area);
                    break;
            }
        }
    };    
    return {
        clickEvents: clickEvents
    };
}(projectData(), functions()));
