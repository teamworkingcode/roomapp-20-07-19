﻿using ModelFiles.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelFiles.Abstract
{
    public abstract class Creator
    {
        public abstract T CreateModel<T>() where T : new();
    }
}
