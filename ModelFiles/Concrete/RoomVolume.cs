﻿using ModelFiles.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelFiles.Enums;
namespace ModelFiles.Concrete
{
    public class RoomVolume : IDimensions
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public float Length { get; set; }
        public Operation Type { get { return Operation.RoomVolume; } }

        public RoomVolume()
        {
                
        }
    }
}
