﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelFiles.Enums
{
    public enum Operation
    {
        Area = 1,
        PaintRequired = 2,
        RoomVolume = 3
    }
}
