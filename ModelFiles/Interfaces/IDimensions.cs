﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelFiles.Enums;
namespace ModelFiles.Interfaces
{
    public interface IDimensions
    {
        float Width { get; set; }
        float Height { get; set; }
        Operation Type { get; }
    }
}
